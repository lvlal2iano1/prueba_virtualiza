<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sistema de empleados Virtualiza</title>
    <link href="{{asset('css/bootstrap.min.css')}}"    rel="stylesheet">
    <link href="{{asset('css/dashboard.css')}}" rel="stylesheet">
    @stack('styles')
    @livewireStyles
</head>
<body>
    <main>
        <header class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
            <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="#">VIRTUALIZA</a>
            <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="navbar-nav">
              <div class="nav-item text-nowrap">
              </div>
            </div>
        </header>
        <div class="container-fluid">
            <div class="row">
                <main>
                    @yield('main')
                </main>
            </div>
        </div>
    </main>
</body>
<script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
@livewireScripts
@stack('footer-scripts')
</html>