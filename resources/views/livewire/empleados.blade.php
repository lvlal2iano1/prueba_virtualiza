<div>
    <div class="row">
        <div class="col-md-10 offset-md-1 mt-5">
        <h3>Sistema de gestion de empleados</h3>
        <div class="mt-5 mb-5">
            <button class="btn btn-info" type="button" data-bs-toggle="modal" data-bs-target="#nuevoEmpleadoModal"> Nuevo empleado</button>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre completo</th>
                <th scope="col">Dirección</th>
                <th scope="col">Localidad</th>
                <th scope="col">Tipo documento</th>
                <th scope="col">Nro documento</th>
                <th scope="col">Codigo Postal</th>
                <th scope="col">Provincia</th>
                <th scope="col">Activo</th>
                <th scope="col">Acciones</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($empleados as $empleado)
                    <tr>
                        <th scope="row">{{$empleado->idLegajo}}</th>
                        <td>{{$empleado->Nombre.' '.$empleado->Apellido}}</td>
                        <td>{{$empleado->Dirección}}</td>
                        <td>{{$empleado->Localidad}}</td>
                        <td>{{$empleado->tipo_documento->tipo_documento}}</td>
                        <td>{{$empleado->NroDocumento}}</td>
                        <td>{{$empleado->CodigoPosta}}</td>
                        <td>{{$empleado->provincia->provincia}}</td>
                        <td>{{$empleado->activo ? 'SI' : 'NO'}}</div>
                        </td>
                        <td>
                            <button class="btn btn-danger btn-sm" onclick="confirmDestroy({{$empleado->idLegajo}})">Eliminar</button>
                        </td>
                    </tr>
                @endforeach

                @if($empleados->count() == 0)
                    <tr>
                        <td colspan="10" class="text-center">No hay empleados registrados</td>
                    </tr>
                @endif
            
            </tbody>
        </table>
        </div>
    </div>

    <!-- Modal -->
    <div wire:ignore.self class="modal fade" id="nuevoEmpleadoModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="false">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Nuevo Empleado</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="mb-2">
                    <label for="Nombre" class="form-label">Nombre</label>
                    <input wire:model.defer="nombre" type="text" class="form-control" id="Nombre">
                </div>
                @error('nombre') <span class="text-danger error">{{ $message }}</span>@enderror
                
                <div class="mb-2">
                    <label for="Apellido" class="form-label">Apellido</label>
                    <input wire:model.defer="apellido" type="text" class="form-control" id="Apellido" required>
                </div>
                @error('apellido') <span class="text-danger error">{{ $message }}</span>@enderror

                <div class="mb-2">
                    <label for="tipo_documento" class="form-label">Tipo de documento</label>
                    <select wire:model.defer="tipoDocumento" class="form-select" id="tipo_documento" aria-label="Default select example">
                        <option value="0" selected>Selecciona un tipo</option>
                        @foreach ($tipos_documento as $tipo_documento)
                            <option value="{{$tipo_documento->id_tipo_documento}}">{{$tipo_documento->tipo_documento}}</option>
                        @endforeach
                    </select>
                </div>
                @error('tipoDocumento') <span class="text-danger error">{{ $message }}</span>@enderror

                <div class="mb-2">
                    <label for="NroDocumento" class="form-label">Numero de documento</label>
                    <input wire:model.defer="nroDocumento" type="text" class="form-control" id="NroDocumento" required>
                </div>
                @error('nroDocumento') <span class="text-danger error">{{ $message }}</span>@enderror

                <div class="mb-2">
                    <label for="Dirección" class="form-label">Dirección</label>
                    <input wire:model.defer="direccion" type="text" class="form-control" id="Dirección" required>
                </div>
                @error('direccion') <span class="text-danger error">{{ $message }}</span>@enderror

                <div class="mb-2">
                    <label for="Localidad" class="form-label">Localidad</label>
                    <input wire:model.defer="localidad"  type="text" class="form-control" id="Localidad" required>
                </div>
                @error('localidad') <span class="text-danger error">{{ $message }}</span>@enderror

                <div class="mb-2">
                    <label for="Provincia" class="form-label">Provincia</label>
                    <select wire:model.defer="provincia" class="form-select" id="Provincia" aria-label="Default select example">
                        <option value="0" selected>Selecciona una provincia</option>
                        @foreach ($provincias as $provincia)
                            <option value="{{$provincia->id_provincia}}">{{$provincia->provincia}}</option>
                        @endforeach
                    </select>
                </div>
                @error('provincia') <span class="text-danger error">{{ $message }}</span>@enderror

                <div class="mb-2">
                    <label for="CodigoPostal" class="form-label">Codigo postal</label>
                    <input wire:model.defer="codigoPostal" type="text" class="form-control" id="CodigoPostal" required>
                </div>
                @error('codigoPostal') <span class="text-danger error">{{ $message }}</span>@enderror

                <div class="mb-2 form-check">
                    <input wire:model.defer="activo" type="checkbox" class="form-check-input" id="Activo" checked>
                    <label class="form-check-label" for="exampleCheck1">Activo</label>
                </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
            <button type="button" class="btn btn-success" wire:click.prevent='store'>Guardar</button>

            </div>
        </div>
        </div>
    </div>
</div>
