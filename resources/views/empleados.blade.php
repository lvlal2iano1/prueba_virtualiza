@extends('layouts.app')

@section('main')
    @livewire('empleados')
@endsection

@push('footer-scripts')
<script type="text/javascript">
    window.livewire.on('userStore', () => {
        $('#nuevoEmpleadoModal').modal('hide');
    });

    window.livewire.on('alert', (message) => {
        Swal.fire(
            'Muy Bien!',
            message,
            'success'
        )
    });

    const confirmDestroy = id => {
        Swal.fire({
            icon: 'warning',
            text: 'Confirma la eliminacion de este empleado? esta accion no tiene retroceso',
            showCancelButton: true,
            position: 'center',
            showConfirmButton: true,
        }).then((result) => {
            if (result.isConfirmed) {
                console.log('see');
            livewire.emit('deleteConfirmed',{id:id})
            }
        })
    }
</script>
@endpush