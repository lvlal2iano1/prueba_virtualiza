## Prueba tecnica Mariano Paniagua para Virtualiza.

## Introduccion:

Leguajes y librerias utilizadas:
    - PHP8
    - Laravel 8
    - Mysql 5.7
    - Livewire 2.8
    - Docker
    - Compiladores: (make, composer)
    - GIT

Aclaracion: 
1 - Se presentará dos modos de instalar el proyecto, una manual y una automatica, ya que en la consigna se planteo que se debe presentar listo para un deploy en un servidor y esto es bastante amplio.

2 - Con el objeto de seguir la consigna lo mas posible, no utilice el mecanismo de migracion que trae Laravel, por ello es que tanto en la instalacion manual como la automatica (Presentadas mas adelante) se crea la base de datos e importa un archivo .sql como se indico. (el mismo se encuentra en db/backups/scheme.sql)

## Instalacion automatica.
Dependencias: Git, Docker, Docker-compose y Make(Instalado por defecto en cualquier distro de linux))

1 - cd path_del_proyect.
2 - https://gitlab.com/lvlal2iano1/prueba_virtualiza.git.
3 - make init (Esto puede demorar unos minutos).
4 - Ya deberia estar disponible el sistema en localhost:8000

Datos para debugar la base de datos: 
    server-host: localhost
    puerto: 33006 (no es un error tiene un 0 mas)
    usuario: developer
    pass: developer
    database: TEST

## Instalacion manual.
Via email os pase un zip con el arbol de archivos y el .sql
Es un proyecto Laravel 8, simplemente deben instalarselo en un virtual host
y lanzarlo como cualquier proyecto Laravel en un servidor Apache o Nginx.


Muchas Gracias!

Mariano Paniagua.
