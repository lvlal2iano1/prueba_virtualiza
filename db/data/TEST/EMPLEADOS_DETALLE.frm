TYPE=VIEW
query=select `TEST`.`empleados`.`idLegajo` AS `idLegajo`,`TEST`.`empleados`.`Apellido` AS `Apellido`,`TEST`.`empleados`.`Nombre` AS `Nombre`,`TEST`.`empleados`.`Dirección` AS `Dirección`,`TEST`.`empleados`.`Localidad` AS `Localidad`,`TEST`.`empleados`.`ID_TIPO_DOCUMENTO` AS `ID_TIPO_DOCUMENTO`,`TEST`.`empleados`.`ID_TIPO_DOCUMENTO` AS `TIPO_DOCUMENTO`,`TEST`.`tipo_documento`.`tipo_documento` AS `DESC_DOCUMENTO`,`TEST`.`empleados`.`NroDocumento` AS `NroDocumento`,`TEST`.`empleados`.`CodigoPostal` AS `CodigoPostal`,`TEST`.`empleados`.`ID_PROVINCIA` AS `ID_PROVINCIA`,`TEST`.`provincia`.`provincia` AS `PROVINCIA` from ((`TEST`.`empleados` join `TEST`.`tipo_documento` on((`TEST`.`tipo_documento`.`id_tipo_documento` = `TEST`.`empleados`.`ID_TIPO_DOCUMENTO`))) join `TEST`.`provincia` on((`TEST`.`provincia`.`id_provincia` = `TEST`.`empleados`.`ID_PROVINCIA`))) order by `TEST`.`empleados`.`idLegajo` desc
md5=53fbe1b8b8b81adfe52f8abaca0e7d4b
updatable=1
algorithm=0
definer_user=developer
definer_host=%
suid=2
with_check_option=0
timestamp=2021-12-16 22:21:43
create-version=1
source=SELECT \n`empleados`.`idLegajo`, \n`empleados`.`Apellido`, \n`empleados`.`Nombre`, \n`empleados`.`Dirección`,\n`empleados`.`Localidad`,\n`empleados`.`ID_TIPO_DOCUMENTO`,\n`empleados`.`ID_TIPO_DOCUMENTO` as \'TIPO_DOCUMENTO\',\n`tipo_documento`.`tipo_documento` as \'DESC_DOCUMENTO\',\n`empleados`.`NroDocumento`,\n`empleados`.`CodigoPostal`,\n`empleados`.`ID_PROVINCIA`,\n`provincia`.`provincia` AS \'PROVINCIA\'  \nFROM `empleados`\nINNER JOIN `tipo_documento` ON `tipo_documento`.`id_tipo_documento` = `empleados`.`ID_TIPO_DOCUMENTO`\nINNER JOIN `provincia` ON `provincia`.`id_provincia` = `empleados`.`ID_PROVINCIA`\nORDER BY `empleados`.`idLegajo` DESC
client_cs_name=utf8
connection_cl_name=utf8_general_ci
view_body_utf8=select `TEST`.`empleados`.`idLegajo` AS `idLegajo`,`TEST`.`empleados`.`Apellido` AS `Apellido`,`TEST`.`empleados`.`Nombre` AS `Nombre`,`TEST`.`empleados`.`Dirección` AS `Dirección`,`TEST`.`empleados`.`Localidad` AS `Localidad`,`TEST`.`empleados`.`ID_TIPO_DOCUMENTO` AS `ID_TIPO_DOCUMENTO`,`TEST`.`empleados`.`ID_TIPO_DOCUMENTO` AS `TIPO_DOCUMENTO`,`TEST`.`tipo_documento`.`tipo_documento` AS `DESC_DOCUMENTO`,`TEST`.`empleados`.`NroDocumento` AS `NroDocumento`,`TEST`.`empleados`.`CodigoPostal` AS `CodigoPostal`,`TEST`.`empleados`.`ID_PROVINCIA` AS `ID_PROVINCIA`,`TEST`.`provincia`.`provincia` AS `PROVINCIA` from ((`TEST`.`empleados` join `TEST`.`tipo_documento` on((`TEST`.`tipo_documento`.`id_tipo_documento` = `TEST`.`empleados`.`ID_TIPO_DOCUMENTO`))) join `TEST`.`provincia` on((`TEST`.`provincia`.`id_provincia` = `TEST`.`empleados`.`ID_PROVINCIA`))) order by `TEST`.`empleados`.`idLegajo` desc
