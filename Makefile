#!/bin/bash
DOCKER_PHP = prueba_virtualiza_myapp_1
DOCKER_DB = prueba_virtualiza_mysql_1
DB_PASS = developer
DB_USER = developer

init: ## Init the project
	clear
	docker-compose up --force-recreate
	docker exec -it ${DOCKER_PHP} composer install
	docker exec -it ${DOCKER_PHP} cp .env.example .env
	docker exec -it ${DOCKER_PHP} php artisan key:generate
	docker exec -it ${DOCKER_PHP} composer dump-autoload --optimize
	docker exec -it ${DOCKER_DB} mysql -u ${DB_USER} -p${DB_PASS} -e "CREATE DATABASE IF NOT EXISTS TEST CHARACTER SET utf8"; 
	docker exec -it ${DOCKER_DB} mysql -u ${DB_USER} -p${DB_PASS} TEST < /backups/scheme.sql --default-character-set=utf8

run: ## Run the containers
	clear
	docker network create proxy || true
	docker-compose up -d --remove-orphans

stop: ## Stop the containers
	clear
	docker-compose stop

ssh: ## ssh's into the PHP container
	clear
	docker exec -it  ${DOCKER_PHP} bash

stats: ## See container's stats
	clear
	docker container stats

buil-test: ## Build test database, migrate and seed it

test: ## Run test's application
	clear
	docker exec -it ${DOCKER_PHP} php artisan test --testdox

restart: ## Restart the containers
	clear
	make stop
	make run

prod: ## Execute production compilations
	clear
	docker exec -it ${DOCKER_PHP} npm run prod

watch: ## Execute npm run watch on php container
	clear
	docker exec -it ${DOCKER_PHP} npm run watch
