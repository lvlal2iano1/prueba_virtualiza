<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Empleados as EmpleadosModel;
use App\Models\Provincia;
use App\Models\TipoDocumento;

class Empleados extends Component
{
    public $nombre, $apellido, 
    $direccion, $localidad, $nroDocumento, 
    $tipoDocumento, $codigoPostal, $provincia;

    public $activo = true;

    public $modalStore = false;

    protected $listeners = ['deleteConfirmed' => 'destroy'];

    protected $rules = [
        'nombre' => 'required|regex:/^[a-zA-ZÀ-ÿ0-9\s.`\']+$/',
        'apellido' => 'required|regex:/^[a-zA-ZÀ-ÿ0-9\s.`\']+$/',
        'direccion' => 'required|regex:/^[a-zA-ZÀ-ÿ0-9\s.`\']+$/',
        'localidad' => 'required|regex:/^[a-zA-ZÀ-ÿ0-9\s.`\']+$/',
        'nroDocumento' => 'required|numeric|regex:/^[a-zA-ZÀ-ÿ0-9\s.`\']+$/',
        'tipoDocumento' => 'required|regex:/^[a-zA-ZÀ-ÿ0-9\s.`\']+$/',
        'codigoPostal' => 'required|regex:/^[a-zA-ZÀ-ÿ0-9\s.`\']+$/',
        'provincia' => 'required|regex:/^[a-zA-ZÀ-ÿ0-9\s.`\']+$/',
    ];

    public function store(){

        $this->validate();

        EmpleadosModel::create([
            'Apellido' => $this->apellido,
            'Nombre' => $this->nombre,
            'Dirección' => $this->direccion,
            'Localidad' => $this->localidad,
            'ID_TIPO_DOCUMENTO' => $this->tipoDocumento,
            'NroDocumento' => $this->nroDocumento,
            'CodigoPostal' => $this->codigoPostal,
            'ID_PROVINCIA' => $this->provincia,
            'activo' => $this->activo,
        ]);

        $this->reset(['nombre','apellido','direccion','localidad','nroDocumento','tipoDocumento','codigoPostal','provincia']);
        
        $this->emit('userStore');
        $this->emit('alert', 'Se ha creado un nuevo empleado exitosamente!');
    }

    public function render()
    {
        $empleados = EmpleadosModel::all();
        $provincias = Provincia::all();
        $tipos_documento = TipoDocumento::all();
        return view('livewire.empleados',  compact('empleados','provincias','tipos_documento'));
    }

    public function destroy($payload){
        return EmpleadosModel::destroy($payload['id']);
    }
}
