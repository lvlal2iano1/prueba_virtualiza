<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Provincia;
use App\Models\TipoDocumento;

class Empleados extends Model
{
    use HasFactory;

    protected $table = 'empleados';

    protected $primaryKey = 'idLegajo';

    protected $fillable = [
        'Apellido',
        'Nombre',
        'Dirección',
        'Localidad',
        'ID_TIPO_DOCUMENTO',
        'NroDocumento',
        'CodigoPostal',
        'ID_PROVINCIA',
        'activo'
    ];

    public $timestamps = false;

    public function provincia(){
        return $this->belongsTo(Provincia::class, 'ID_PROVINCIA', 'id_provincia');
    }

    public function tipo_documento(){
        return $this->belongsTo(TipoDocumento::class, 'ID_TIPO_DOCUMENTO', 'id_tipo_documento');
    }
}
