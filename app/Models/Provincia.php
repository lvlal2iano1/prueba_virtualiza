<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Empleados;

class Provincia extends Model
{
    use HasFactory;

    protected $table = 'provincia';

    public function empleados(){
        return $this->hasMany(Empleados::class, 'id_provincia', 'idLegajo');
    }
}
